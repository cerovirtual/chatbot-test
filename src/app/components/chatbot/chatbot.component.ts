import {Component, OnInit} from '@angular/core';

import {ChatManagerService} from '../../services/chat-case/chat-manager.service';

@Component({
  selector: 'app-chatbot',
  templateUrl: './chatbot.component.html',
  styleUrls: ['./chatbot.component.css']
})
export class ChatbotComponent implements OnInit {

  constructor(private _chatManager: ChatManagerService) { }

  ngOnInit() {
  }
}
