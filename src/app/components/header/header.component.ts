import {Component, OnInit} from '@angular/core';

import {ChatManagerService} from '../../services/chat-case/chat-manager.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  titleText: string;
  titleImage: string;

  constructor(private _chatManager: ChatManagerService) {
  }

  ngOnInit() {
    this.titleText = this._chatManager.chatCase['header'].title;
    this.titleImage = this._chatManager.chatCase['header'].image;
  }
}
