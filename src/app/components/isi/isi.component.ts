import {Component, OnInit} from '@angular/core';

import {ChatManagerService} from '../../services/chat-case/chat-manager.service';

@Component({
  selector: 'app-isi',
  templateUrl: './isi.component.html',
  styleUrls: ['./isi.component.css']
})
export class IsiComponent implements OnInit {

  isiTitle: string;
  isiText: string;
  displayIsi: boolean;

  constructor(private _chatManager: ChatManagerService) {
  }

  ngOnInit() {
    this.isiTitle = this._chatManager.chatCase['isi'].title;
    this.isiText = this._chatManager.chatCase['isi'].text;
    this.displayIsi = this._chatManager.chatCase['isi'].display;
  }
}
