import {Component, OnInit} from '@angular/core';

import {ChatManagerService} from '../../services/chat-case/chat-manager.service';

@Component({
  selector: 'app-prompts',
  templateUrl: './prompts.component.html',
  styleUrls: ['./prompts.component.css']
})
export class PromptsComponent implements OnInit {

  promptsLegend: string;

  constructor(private _chatManager: ChatManagerService) {
  }

  ngOnInit() {
    this.promptsLegend = this._chatManager.chatCase['prompts'].legend;
  }
}
