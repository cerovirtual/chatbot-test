import { Injectable } from '@angular/core';

import {ChatProviderService} from '../../services/chat-case/chat-provider.service';

@Injectable()
export class ChatManagerService {
  caseCard = [];
  chatCase: any;
  chatHistory: any[] = [];
  completedDialog = [];
  idleMessage = setTimeout( () => {
    this.addMessage('bot-reply', this.messages.idle[Math.round(Math.random() * (this.messages.idle.length - 1))]);
  }, 6000);
  messages: any;
  suggestions = [];

  constructor(private _chatProvider: ChatProviderService) {
    this.chatCase = this._chatProvider.getChatCase();
    this.checkChat(
      this.chatCase['chat']
    );
    this.startCase();
  }

  private checkChat(c) {
    this.chatCase['chat'].forEach( (c) => {
      this.caseCard[c.id] = c;
      // console.log(c.id + ': ' + c.title);
    });

    const idArray = new Array(c.length);
    const suggestArray = [];
    c.forEach( (id) => {
      idArray[id.id] = idArray[id.id] + 1;
      id.suggest.forEach(function (s) {
        suggestArray.push(s);
      });
    });

    // Check for unreferenced
    c.forEach( (id) => {
      if (suggestArray.indexOf(id.id) === -1) {
        console.log(`card id ${ id.id } is not referenced by suggestion`);
      }
    });

    // Check for null references
    c.forEach( (id) => {
      id.suggest.forEach( (s) => {
        if (suggestArray.indexOf(s) === -1) {
          console.log(`card id ${ id.id } is referenced but doesn't exist`);
        }
      });
    });

    // Check for dupes
    idArray.forEach( (i) => {
      if (i > 1) {
        console.log(`card id ${ i } is duplicate`);
      }
    });

    // Messages
    this.messages = this.chatCase['messages'];
  }

  private startCase() {
    this.addMessage('bot-reply', this.caseCard[0].answer);
    this.loadSuggestions(this.caseCard[0].suggest);
    // this.loadFullCase();
  }

  private loadFullCase() {
    this.caseCard.forEach((c) => {
      if (c.prompt) {
        this.addMessage('user-message', c.prompt);
      }
      if (c.answer) {
        this.addMessage('bot-reply', c.answer);
      }
    });
  }

  addMessage(messageClass, messageText) {
    const item = {
      'messageClass': messageClass,
      'messageText': messageText
    };
    this.chatHistory.push(item);
  }

  loadSuggestions(suggestions) {
    this.suggestions = [];
    let sCount = 2;
    suggestions.forEach( (s) => {
      if (this.completedDialog.indexOf(s) === -1) {
        this.suggestions.push(s);
        sCount++;
      }
    });

    if (sCount === 2) {
      this.addMessage('small', 'You have reached the end of this case. Tap NEXT to continue.');
      clearInterval(this.idleMessage);
    }
  }

  addPrompt(prompt) {
    this.completedDialog.push(parseInt(prompt));
    this.addMessage('user-message', this.caseCard[prompt].prompt);

    if (this.caseCard[prompt].payload) {
      this.addPayload('user-message', this.caseCard[prompt].payload);
    }

    setTimeout( () => {
      if (this.caseCard[prompt].answer !== '') {
        this.addMessage('bot-reply', this.caseCard[prompt].answer);
        this.loadSuggestions(this.caseCard[prompt].suggest);
      }
    }, 800 + (800 * Math.random()));

    clearInterval(this.idleMessage);

    this.idleMessage = setTimeout( () => {
      this.addMessage('bot-reply', this.messages.idle[Math.round(Math.random() * (this.messages.idle.length - 1))]);
    }, 9000 + (1000 * Math.random()));

    this.updateProgress();
  }

  removeSuggestClass(event: any) {
    event.target.classList.add('zoomOut');
  }

  updateProgress() {
    // console.log('Progress: ', Math.ceil(Math.max.apply(null, this.completedDialog) / 10));
    for (let i = 0; i < Math.ceil(Math.max.apply(null, this.completedDialog) / 10); i++) {
      document.getElementById('progressbar').getElementsByTagName('span')[i].setAttribute('class', 'green');
    }
  }

  addPayload(messageClass, messagePayload) {
    const item = {
      'messageClass': messageClass,
      'messagePayload': messagePayload
    };
    this.chatHistory.push(item);
  }

  zoomImage(src) {
    /*const n = document.createElement('div');
    const i = document.createElement('img');
    i.setAttribute('src', src);
    n.appendChild(i);
    n.onclick = function () {
      this.setAttribute('class', 'magnify animated zoomOut');
      this.parentNode.removeChild(document.getElementsByClassName('magnify')[0]);
    }
    n.setAttribute('class', 'magnify animated zoomIn');
    document.getElementById('machine').appendChild(n);*/
    console.log('TODO', 'Implement Zoom Feature :P');
  }
}
