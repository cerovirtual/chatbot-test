import {TestBed, inject} from '@angular/core/testing';

import {ChatProviderService} from './chat-provider.service';

describe('ChatProviderService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ChatProviderService]
    });
  });

  it('should be created', inject([ChatProviderService], (service: ChatProviderService) => {
    expect(service).toBeTruthy();
  }));
});
