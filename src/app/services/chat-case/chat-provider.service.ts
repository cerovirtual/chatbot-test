import {Injectable} from '@angular/core';
import {Http, Headers, Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class ChatProviderService {
  /**
   0- welcome
   1 - 9 chief complaint, symptoms
   10- 19 medical history
   20- 29 personal, family history, demographics
   30- 39 labs, imaging, current medications
   40- 49 knowledge and awareness of condition, treatments
   50- 59 treatment recommendations and followup
   **/
  private CASE_CHAT: any = {
    'header': {
      'title': 'You are chatting with patient Charlie. Tap green prompts below to ask Charlie some questions to diagnose and treat his case.',
      'image': 'http://brycesady.com/651/casechat/charlie.png'
    },
    'chat': [
      {
        'id': 0,
        'title': 'welcome',
        'prompt': '',
        'answer': 'Hi Doctor, my name is Charlie. Thanks for seeing me today.',
        'suggest': [1, 10, 20]
      },
      {
        'id': 1,
        'title': 'chief complaint',
        'prompt': 'Hi Charlie, what seems to be bothering you today?',
        'answer': 'I\'ve been having trouble breathing lately.',
        'suggest': [1, 2, 3, 4, 5, 6, 10, 20]
      },
      {
        'id': 2,
        'title': 'onset of symptoms',
        'prompt': 'That doesn\'t sound good. When did this start?',
        'answer': 'Well, I noticed it two weeks ago while I was going for a walk up a hill behind my house.',
        'suggest': [1, 2, 3, 4, 5, 6, 10, 20]
      },
      {
        'id': 3,
        'title': 'symptom triggers',
        'prompt': 'When does this seem to happen?',
        'answer': 'If I go up stairs or walk up a hill I seem to lose my breath. If I\'m still or watching TV, nothing happens.',
        'suggest': [1, 2, 3, 4, 5, 6, 10, 20]
      },
      {
        'id': 4,
        'title': 'duration of symptoms',
        'prompt': 'Now do you notice how long until your breathing goes back to normal?',
        'answer': 'When it happens, I\'m beat for day. It takes a long time.',
        'suggest': [1, 2, 3, 4, 5, 6, 10, 20]
      },
      {
        'id': 5,
        'title': 'severity of symptoms',
        'prompt': 'So on a scale from 0 to 10, where 0 is no trouble breathing at all and 10 is unable to breathe at all, how severe would you say your symptoms are?',
        'answer': 'Usually it\'s like a 2, but when I go for a walk or something, it\'s a 6 or 7.',
        'suggest': [1, 2, 3, 4, 5, 6, 10, 20]
      },
      {
        'id': 6,
        'title': 'impact on quality of life',
        'prompt': 'So is this causing you to decrease your activity or change your habits at all?',
        'answer': 'Well I\'m definitely not exercising anymore and I think twice before taking the stairs.',
        'suggest': [1, 2, 3, 4, 5, 6, 10, 20]

      },
      {
        'id': 10,
        'title': 'smoking status',
        'prompt': 'Do you smoke now, or have you ever smoked cigarettes or cigars or a pipe?',
        'answer': 'I quite smoking about ten years ago. I probably smoked a pack every week for twenty years before that.',
        'suggest': [1, 10, 11, 12, 13, 14, 15, 10, 20, 30]
      },
      {
        'id': 11,
        'title': 'hospitalizations',
        'prompt': 'Have you ever been hospitalized for this before, or for anything else?',
        'answer': 'I threw my back out a few times and had to go to the ER. About two years ago I had a mini-stroke and spent a night in the hospital so they could watch me.',
        'suggest': [1, 10, 11, 12, 13, 14, 15, 10, 20, 30]
      },
      {
        'id': 12,
        'title': 'chronic conditions',
        'prompt': 'Do you have any other chronic diseases such as hypertension, depression, or anything like that?',
        'answer': 'I have high blood pressure, if that\'s what you mean. I also have AF and had a mini-stroke about two years ago.',
        'suggest': [1, 10, 11, 12, 13, 14, 15, 10, 20, 30]
      },
      {
        'id': 13,
        'title': 'general health assessment',
        'prompt': 'Other than this breathing issue we are talking about today, how are you feeling?',
        'answer': 'I\'m a little lightheaded, to be honest.',
        'suggest': [1, 10, 11, 12, 13, 14, 15, 10, 20, 30]
      },
      {
        'id': 14,
        'title': 'allergies, drug reactions',
        'prompt': 'Do you have any allergies or have you ever had a drug reaction?',
        'answer': 'No. I avoid dairy, but it\'s not really an allergy.',
        'suggest': [1, 10, 11, 12, 13, 14, 15, 10, 20, 30]
      },
      {
        'id': 15,
        'title': 'mold, dust, or pets',
        'prompt': 'Any mold, excess dust, or pets at home or work?',
        'answer': 'No mold that I know of or dust. We keep a cat, but she\'s easy to cleanup after.',
        'suggest': [1, 10, 11, 12, 13, 14, 15, 10, 20, 30]
      },
      {
        'id': 20,
        'title': 'age',
        'prompt': 'Charlie, when were you born?',
        'answer': '1953. I\'m 64.',
        'suggest': [1, 10, 20, 21, 22, 30]

      },
      {
        'id': 21,
        'title': 'occupation',
        'prompt': 'Charlie, what do you do for work or are you retired now?',
        'answer': 'I\'m an insurance adjuster. I\'ve done that for about 25 years. I worked as a school teacher before that.',
        'suggest': [1, 10, 20, 21, 22, 30]
      },
      {
        'id': 22,
        'title': 'family medical history',
        'prompt': 'Can you tell me about your parents? How long did they live and how did they pass?',
        'answer': 'My dad died in a construction accident when I was a teenager. My mom died of cancer when she was 74.',
        'suggest': [1, 10, 20, 21, 22, 30]
      },
      {
        'id': 30,
        'title': 'current acute treatment',
        'prompt': 'Do you take anything to help with breathing when this happens?',
        'answer': 'Yes, I have an inhaler. I think it\'s albuterol.',
        'suggest': [1, 10, 20, 30, 31, 32, 33, 34, 40]
      },
      {
        'id': 31,
        'title': 'current maintenance treatment',
        'prompt': 'Are you taking anything daily to prevent any breathing issues?',
        'answer': 'I take medicine for blood pressure, but not breathing.',
        'suggest': [1, 10, 20, 30, 31, 32, 33, 34, 40]
      },
      {
        'id': 32,
        'title': 'laboratory tests',
        'prompt': 'Let me bring up your lab tests.',
        'answer': '',
        'payload': 'http://brycesady.com/651/casechat/labs.png',
        'suggest': [1, 10, 20, 30, 31, 32, 33, 34, 40]
      },
      {
        'id': 33,
        'title': 'imaging',
        'prompt': 'Let me have a look at your recent CT scans.',
        'answer': '',
        'payload': 'http://brycesady.com/651/casechat/ct.png',
        'suggest': [1, 10, 20, 30, 31, 32, 33, 34, 40]
      },
      {
        'id': 34,
        'title': 'medication list',
        'prompt': 'It says here that you are taking lisinopril or Zestril and apixaban or Eliquis. Is there anything else?',
        'answer': 'I take some vitamins and sometimes something to help me get to sleep at night.',
        'suggest': [1, 10, 20, 30, 31, 32, 33, 34, 40]
      },
      {
        'id': 40,
        'title': 'expectations',
        'prompt': 'Charlie, what are you hoping we can accomplish in today\'s session?',
        'answer': 'Well this breathing issue has me worried.  What if I stop breathing? I feel like I need something stronger.',
        'suggest': [1, 10, 20, 30, 40, 41]
      },
      {
        'id': 41,
        'title': 'diagnosis',
        'prompt': 'So I think what you have is \'Idiopathic Pulmonary Fibrosis\', or IPF. I should tell you this is a pretty serious diagnosis, and you can expect it to get worse. However there may be somethings we can do. ',
        'answer': 'That does sound serious.  Can you tell me more?',
        'suggest': [42]
      },
      {
        'id': 42,
        'title': 'more',
        'prompt': 'Typically all patients have decreasing ability to use their lungs to breathe. Everyone\'s experience is a little bit different, in terms of how long they have to live. In general, though, the earlier we start treatment, the longer you\'ll live.',
        'answer': 'About how long do I have?',
        'suggest': [40, 43, 51, 53]
      },
      {
        'id': 43,
        'title': 'prognosis',
        'prompt': 'Have a look at this chart. The patients who were treated right away are this black line. More than 60% of them lived more than 5 years.  This is why we want to consider a treatment for you today.',
        'payload': 'http://brycesady.com/651/casechat/ipfsurv.png',
        'answer': 'I see.',
        'suggest': [40, 43, 51, 53]
      },
      {
        'id': 51,
        'title': 'knowledge of pirfenidone',
        'prompt': 'We have a treatment called pirfenidone that we can try. This will reduce how fast the disease progresses. Right now we\'re measuring that with the FVC (or Forced Vital Capacity) test that I showed you in your labs. In a clinical trial of pirfenidone, it reduced the number of people who\'s FVC declined, it reduced the number of people who couldn\'t complete a 6 minute walk, which sounds like one of the things you were concerned about.',
        'answer': 'What kind of side effects are there?',
        'suggest': [52]
      },
      {
        'id': 52,
        'title': 'pirfenidone side effects',
        'prompt': 'So some patients experience nausea on pirfenidone. You can alleviate that by taking it with meals. Another serious side effect is the possibility of sun burns. I want to encourage you to continue to take your walks, but when you go out, you should wear a hat, thin gloves, long sleeve shirts, and sunscreen with high UV protection. Right after you take the medicine, you\'ll want to avoid sunlight altogether.',
        'answer': 'Okay. This sounds like an option.  What next?',
        'suggest': [60, 53, 61, 62]
      },
      {
        'id': 53,
        'title': 'knowledge of nintedanib',
        'prompt': 'There is a treatment for IPF, nintedanib, which we can try. We measure the progress of IPF with FVC or the Forced Vital Capacity test that I showed you in your labs. In clinical trials, nintedanib slowed lung function decline by about 50%. So what it will do is keep the disease from getting worse too quickly.',
        'answer': 'Are there any side effects?',
        'suggest': [54]
      },
      {
        'id': 54,
        'title': 'nintedanib side effects',
        'prompt': 'Some patients have gastrointestinal side effets that are mild to moderate.  Diarrhea is the most common.',
        'answer': 'So what\'s next?',
        'suggest': [61, 51, 60, 62]
      },
      {
        'id': 60,
        'title': 'recommend pirfenidone',
        'prompt': 'So I\'m going to start you on pirfenidone. You are going to take eight pills a day. I recommmend 2 at breakfast, 3 at lunch, and 3 with dinner.  Remember, eating these with meals will help you manage the nausea. If it\'s still too much, I can write you a prescription for pantoprozole 40mg, to help with that. You should continue to take all of your other medications. If you get a bad skin rash, you should call me right away. Do you have any questions?',
        'answer': 'When should I see you next?',
        'suggest': [63]
      },
      {
        'id': 61,
        'title': 'recommend nintedanib',
        'prompt': 'I\'m going to recommend nintedanib. You will take one capsule twice a day. I recommend one with breakfast and another after dinner. Let me know if the gastrointesintal issues are too much to handle.',
        'answer': 'Okay. So when should I come back?',
        'suggest': [63]
      },
      {
        'id': 62,
        'title': 'recommend no change',
        'prompt': 'At this time, although there are treatment options of IPF, I think it\'s best to wait. There may be better options in the future, or we might find that that your diagnosis changes. You should definitely let me know if the breathing issues become worse, and I recommend you keep a diary of every time you become breathless, what time of day it was, how long the symptom lasted, and what you think might have caused it. This may help us find an appropriate treatment for you.',
        'answer': 'Should I come back?',
        'suggest': [63]
      },
      {
        'id': 63,
        'title': 'followup recommendations',
        'prompt': 'We will meet again in three months to see how you are progressing. Of course, if anything happens in the meantime, you should call my office right away.',
        'answer': 'Thanks, Doctor. I\'ll make an appointment later today.',
        'suggest': []
      }
    ],
    'messages': {
      'idle': [
        'Do you have anything else to ask me?',
        'Do you have any questions for me?',
        'Is there anything else?',
        'Do you want to ask me anything else?',
        'What else can I tell you?'
      ],
      'done': [
        'I think we\'re done here.',
        'Can I go now?',
        'I\'ll call you if there\'s anything else.  Can I leave now?'
      ]
    },
    'prompts': {
      'legend': 'Ask Charlie a question about...'
    },
    'isi': {
      'display': true,
      'title': 'IMPORTANT SAFETY INFORMATION',
      'text': 'Lorem ipsum dolor sit amet, et euismod interesset per, ullum numquam eruditi ne mel. At harum meliore mel, ea dolores ocurreret reformidans qui. Vim alterum fabulas utroque cu, bonorum admodum deterruisset eu ius. Et sed velit propriae, ad per summo primis debitis. Fabulas adipiscing mea eu. Has vocibus oporteat te, doming oblique no qui, verear offendit eam et. An his habemus antiopam.'
    }
  };

  constructor() { }

  getChatCase(): any {
    return this.CASE_CHAT;
  }
}
