import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';

/* Components */
import {AppComponent} from './app.component';
import {ChatbotComponent} from './components/chatbot/chatbot.component';
import {HeaderComponent} from './components/header/header.component';
import {ProgressComponent} from './components/progress/progress.component';
import {PromptsComponent} from './components/prompts/prompts.component';
import {IsiComponent} from './components/isi/isi.component';

/* Services */
import {ChatProviderService} from './services/chat-case/chat-provider.service';
import {ChatManagerService} from './services/chat-case/chat-manager.service';

@NgModule({
  declarations: [
    AppComponent,
    ChatbotComponent,
    HeaderComponent,
    ProgressComponent,
    PromptsComponent,
    IsiComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [
    ChatProviderService,
    ChatManagerService
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule {
}
